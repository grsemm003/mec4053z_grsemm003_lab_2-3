// Description----------------------------------------------------------------|
/*
 * Initialises a struct with Name and Age data. Displays results on LEDs and
 * LCD.
 */
// DEFINES AND INCLUDES-------------------------------------------------------|

#define STM32F051												   //COMPULSORY
#include "stm32f0xx.h"											   //COMPULSORY
#include <lcd_stm32f0.h>
#include<string.h>


// GLOBAL VARIABLES ----------------------------------------------------------|
//Declare your struct here.
struct nameStruct{
		uint8_t 	age;
		char		name[4];
	};

uint32_t count;
uint8_t i, namelcd;



// FUNCTION DECLARATIONS -----------------------------------------------------|

void main(void);                                                   //COMPULSORY
void ResetClockTo48Mhz(void);									   //COMPULSORY
void InitLEDs(void);                                               //COMPULSORY
void DisplayOnLEDs(uint32_t number);                               //COMPULSORY
void Delay(void);


// MAIN FUNCTION -------------------------------------------------------------|

void main(void)
{
	ResetClockTo48Mhz();                       					   //COMPULSORY
	InitLEDs();                                                    //COMPULSORY
	lcd_init();
	lcd_command(LCD_CLEAR_DISPLAY);
	lcd_command(LCD_FOUR_BIT_TWO_LINE_MODE);


	//Initialise your struct here.

	struct nameStruct emma={22, {'E','m','m','a'}};
	lcd_two_line_write("Emma","22");

	while(1)
	{

		// Write your algorithm to increment the LEDs here.
		for (count = 0; count <= emma.age; count++)
		{
			DisplayOnLEDs(count);
			Delay();
		}

		for (count = emma.age; count > 0; count--)
		{
			DisplayOnLEDs(count);
			Delay();
		}

	}


}

// OTHER FUNCTIONS -----------------------------------------------------------|

/* Description:
 * This function initialises the GPIO to display output data
 */
void InitLEDs(void)												   //COMPULSORY
{											  					   //COMPULSORY
	*(uint32_t *)0x40021014 |= 0x00040000;						   //COMPULSORY
	*(uint32_t *)0x48000400 |= 0x00001555;		     			   //COMPULSORY
}																   //COMPULSORY

/* Description:
 * This function takes in a 8 bit positive number and displays it on the LEDs
 */
void DisplayOnLEDs(uint32_t number)								   //COMPULSORY
{								                                   //COMPULSORY
	*(uint32_t *)0x48000414 = number;                              //COMPULSORY
}                                                                  //COMPULSORY

/* Description:
 * This function resets the STM32 Clocks to 48 MHz
 */
void ResetClockTo48Mhz(void)									   //COMPULSORY
{																   //COMPULSORY
	if ((RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL)			   //COMPULSORY
	{															   //COMPULSORY
		RCC->CFGR &= (uint32_t) (~RCC_CFGR_SW);					   //COMPULSORY
		while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI);	   //COMPULSORY
	}															   //COMPULSORY

	RCC->CR &= (uint32_t)(~RCC_CR_PLLON);						   //COMPULSORY
	while ((RCC->CR & RCC_CR_PLLRDY) != 0);						   //COMPULSORY
	RCC->CFGR = ((RCC->CFGR & (~0x003C0000)) | 0x00280000);		   //COMPULSORY
	RCC->CR |= RCC_CR_PLLON;									   //COMPULSORY
	while ((RCC->CR & RCC_CR_PLLRDY) == 0);						   //COMPULSORY
	RCC->CFGR |= (uint32_t) (RCC_CFGR_SW_PLL);					   //COMPULSORY
	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL);		   //COMPULSORY
}

void Delay(void)
{
	int i,j;
	for (i=0; i<=120; i++)
		for (j=0; j<=30000; j++);
}
